'use strict';

exports.config = {
  //seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
  getPageTimeout: 60000,
  allScriptsTimeout: 500000,
  framework: 'custom',
  useAllAngular2AppRoots: true,
  // path relative to the current config file
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  capabilities: {
    'browserName': 'chrome'
  },

  // Spec patterns are relative to this directory.
  specs: [
    './features/*.feature'
  ],

  baseURL: 'http://localhost:4200/login', 
  cucumberOpts: {
    //require: ['./features/Login_step_definitions.js','support.js','timeout.js'],
	require: ['./features/Login_step_definitions.js','support.js'],
    tags: '@tagThis',
    format: 'pretty'
    //profile: false,
    //no-source': true
  }
};

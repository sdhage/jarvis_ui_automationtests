module.exports = function() {

var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);
var expect = chai.expect;

var userNameElement;
var passwordElement;
	
this.Given(/^I go to (.*)$/, function (url, callback) {		
		console.log(url);
		browser.get(url).then(function(){
		userNameElement = element.all(by.id('userName')).first();
		passwordElement = element.all(by.id('pwd')).first(); 		
		browser.waitForAngular();    				
		callback();
	   });
});
 
this.When(/^I add Login and Password in the respective field (.*)$/, function (userName, callback) {
		userNameElement.sendKeys(userName);
        passwordElement.sendKeys(userName);  
		callback();
});

 this.When(/^I click the Login button$/, function (callback) {
		var button=element(by.buttonText('Login'));
		button.click();        
		browser.waitForAngular();    
		callback();
 }); 
 
  this.Then(/^I should see Home Page (.*)$/, function (homeUrl,callback) {
    browser.getCurrentUrl().then(function(url) {
		expect(url).to.equal(homeUrl);
		callback();
	});  	
  });
 
  this.When(/^I click the Patient Registration link button$/, function (callback) {
	  element(by.css('a[href*="patientregistration"]')).click().then(function(){
	  callback();
  });  
 });

 
 this.When(/^I enter First Name$/, function (callback) {
  element(by.css("input[formControlName=firstName]")).sendKeys('First name').then(function(){
  callback();
  });
  
});

this.When(/^I enter Last Name$/, function (callback) {
  element(by.css("input[formControlName=lastName]")).sendKeys('Last name').then(function(){
  callback();
  });  
});
 
 this.When(/^I Select Gender (.*) from dropdown$/, function (gender, callback) {
   element(by.css('[formcontrolname="gender"]')).click().then(function(){
	   element(by.xpath('//option[contains(.,"'+gender+'")]')).click().then(function(){
		  callback();
	   });
   });  
 });
 
 this.When(/^I Select DOB (.*)$/, function (dob,callback) {
  var dobControl=element(by.id("dob"));
  dobControl.sendKeys(dob).then(function(){
  callback();
  })
  
});

this.When(/^I entered Address Line1 (.*) Line2 (.*)$/, function (line1,line2,callback) {
console.log (line1, line2)
 element(by.css('[formcontrolname="line1"]')).sendKeys(line1).then(function(){
 	element(by.css('[formcontrolname="line2"]')).sendKeys(line2).then(function(){
		callback();
	});
 });  
});

 this.When(/^I click the Save button$/, function (callback) {
   element(by.buttonText('Save')).click().then(function(){
      callback();
   })
 });
 
  this.Then(/^I should see Patient Home Page (.*)$/, function (homeUrl,callback) {
   browser.getCurrentUrl().then(function(url) {
		expect(url).to.equal(homeUrl);
		callback();
	}); 
 });
  
  
	// this.registerHandler('AfterFeatures', function(features, next) {

		// var reporter = require('cucumber-html-reporter');
	 
		// var options = {
			// theme: 'foundation',
			// jsonFile: 'cucumber-test-results.json',
			// output: 'cucumber_report.html',
			// reportSuiteAsScenarios: true,
			// launchReport: false
		// };
	 
		// reporter.generate(options);
		// next();
	// }); 
 
};
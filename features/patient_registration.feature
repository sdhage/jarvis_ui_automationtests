Feature: Enter Patient Registration details
	As a user of Patient Registration
    I should be able to Enter Patient details in to Patient Registration Form
    In order to register a new Patient	
	
		
	@tagThis
	Scenario Outline: Enter Patient Registration details 
		Given I go to <url> 
        When I add Login and Password in the respective field <userName>
        And I click the Login button
		And I click the Patient Registration link button
		And I enter First Name
		And I enter Last Name
		And I Select Gender <gender> from dropdown
		And I Select DOB <dob>
		#And I entered Address Line1 <line1> Line2 <line2>
		And I click the Save button 
        Then I should see Patient Home Page <homeUrl>
		Examples:
		|url|gender|dob|line1|line2|userName|homeUrl|
		|http://10.167.178.23/jarvispr|Male|1975/06/12|Pune|India|varianadmin|http://10.167.178.23/jarvispr/home|
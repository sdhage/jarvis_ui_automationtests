Feature: Login to Patient Registration Page
	As a user of Patient Registration
    I should be able to login in to Patient Registration
    In order to register a new Patient
	
	@tagThis
    Scenario Outline: Login In to Patient Registration
	Given I go to <url> 
        When I add Login and Password in the respective field <userName>
        And I click the Login button
        Then I should see Home Page <homeUrl>
	Examples:
	|url|homeUrl|userName|
	|http://10.167.178.23/jarvispr|http://10.167.178.23/jarvispr/home|varianadmin|
	

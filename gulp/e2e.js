'use strict';

var gulp = require('gulp');
var reporter = require('gulp-protractor-cucumber-html-report');
// var argv = require('yargs').argv;
var protractor = require("gulp-protractor");
var replace = require("gulp-replace");
var exec = require('child_process').exec;
var protractordef = require("protractor");
var del =  require("del");


module.exports = function(options) {

    gulp.task('webdriver-update', protractor.webdriver_update);

    gulp.task('Generate-TestResult', function () {
        var resultFile = 'cucumber-test-results.json';
        var reportLocation = './TestResults/';

        gulp.src(resultFile)
            .pipe(replace('][', ','))
            .pipe(gulp.dest('e2e/'))
            .pipe(reporter({
                dest: reportLocation
            }));
    });

    gulp.task('runTests', function () {

        exec('protractor protractor.conf.js',function(err, stdOut, std){
            console.log(err);
            console.log(stdOut);
            console.log(std);
        })
    });

    gulp.task('clean', function (done) {
        del(['cucumber-test-results.json', './TestResults/'], done);
    }); 

    gulp.task( 'run', ['clean','webdriver-update', 'runTests',  'Generate-TestResult']);    
}
